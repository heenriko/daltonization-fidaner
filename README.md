# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* This small application performs a daltonization method proposed by Fidaner.
* A matrix transformation is performed to change the lightness in the selected image,
	*Allowing protanopia observers to see the intended design.
	
* The formula for matrix transformation is proposed from Anagnostopoulos et. al:
 * https://link.springer.com/content/pdf/10.1007%2F978-1-4419-0221-4_35.pdf
 
* version 1.0

### How do I get set up? ###

* Simply run the index.html file to daltonize the selected image

* To change the image: 
	add the image you want to daltonize in directory: fidaner/img/
	go to file: fidaner/js/fidaner.js
		replace file source at line 82 with your image file name (e.g. imageObj.src = 'your_file.extension';)
	
  * If you want to replace the original image
		go to file: fidaner/index.html
		change img src at line 24
	