function RGB_To_LMS(rgb)
{
	var toLMS = math.matrix([ [17.8824, 43.5161, 4.1193], [3.4557, 27.1554, 3.8671], [0.02996, 0.18431, 1.4670] ]); 
	return math.multiply(toLMS, rgb );
}

function LMS_To_LMS_Prot(lms)
{
	var toLmsProt = math.matrix([ [0, 2.02344, -2.52581], [0, 1, 0], [0, 0, 1] ]); 
	return math.multiply(toLmsProt, lms );
}

function LMS_Prot_To_RGB_Prot(lmsProt)
{
	var toRgbProt = math.matrix([ [0.0809, -0.1305, 0.1167], [-0.0102, 0.0540, -0.1136], [-0.0003, -0.0041, 0.6935] ]); 
	return math.multiply(toRgbProt, lmsProt );
}

function RGB_Subtract_RGB_Prot(rgb, rgbp)
{
	return math.subtract(rgb, rgbp);
}

function RGB_add_error(rgb, e)
{
	return math.add(rgb, e);
}

function RGB_ErrMod(rgb)
{
	var errMod = math.matrix([ [0, 0, 0], [0.7, 1, 0], [0.7, 0, 1] ]); 
	return math.multiply(errMod, rgb );
}

var rgb, lms, lmsp, rgbp, dalt, err, errMod;

$(document).ready(function()
{
	$(window).load(function()
	{

		var x = 0;
		var y = 0;	

		var canvas = document.getElementById('canvas');
		var context = canvas.getContext('2d');
	
		var imageObj = new Image();
    imageObj.onload = function()
    {
    	context.drawImage(imageObj, x, y );

    	var imageData = context.getImageData(x, y, imageObj.width, imageObj.height);
      var data = imageData.data;

     for(var i = 0; i < data.length; i += 4) 
     {	
     		rgb = math.matrix([ data[i], data[i + 1], data[i + 2] ]);
				lms = RGB_To_LMS(rgb);
				lmsp = LMS_To_LMS_Prot(lms);
				rgbp = LMS_Prot_To_RGB_Prot(lmsp);
				
				// subtract rgb - rgbp to find error
				err = RGB_Subtract_RGB_Prot(rgb, rgbp)

				// multiply error with error modification
				errMod = RGB_ErrMod(err);

				// add error mod to rgb
				dalt = RGB_add_error(rgb,errMod);
        
        data[i] = dalt._data[0]; 				// red
        data[i + 1] = dalt._data[1]; 		// green
        data[i + 2] = dalt._data[2];  	// blue
      }

      // overwrite original image
      context.putImageData(imageData, x, y);

    };
    
    imageObj.src = 'img/71.png';	// 10 x 10 px.

	});
	
});